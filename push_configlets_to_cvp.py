#!/usr/bin/env python

import requests
import json
import os
import pprint
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import getpass

# Lab - modify for environments
CVP_HOST = "192.168.1.248"

# Prompt user and password
CVP_USER = raw_input("Username: ")
CVP_PWD = getpass.getpass("Password: ")

CONFIGLET_SUBDIR = 'configlets'   # subdirectory containing configlet text files

# To remove warnings
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Login to CVP
auth_data = json.dumps({"userId": CVP_USER, "password": CVP_PWD})
auth_url = "https://%s/cvpservice/login/authenticate.do" % CVP_HOST
auth_response = requests.post(auth_url, data=auth_data, verify=False)
assert auth_response.ok
cookies = auth_response.cookies

def search_for_configlet(query):

        # configlet_search_url = ('https://%s/cvpservice/configlet/searchConfiglets.do?queryparam=%s&startIndex=%d&endIndex=%d' %(CVP_HOST, query, start, end))
        configlet_search_url = ('https://%s/cvpservice/configlet/getConfigletByName.do?name=%s' %(CVP_HOST, query))
        configlet_search_response = requests.get(configlet_search_url, data=None, cookies=cookies, verify=False)
        configlet_list_json = configlet_search_response.json()

        # Check to see if there is a valid key for configlet
        # Existing configlet will have dict key = 'key'
        # If the configlet does not exist it will have the following dict keys:
        # {
        #     "errorCode": "132801",
        #     "errorMessage": "Entity does not exist"
        # }

        if 'errorCode' in configlet_list_json:
                # If the configlet does exist, errorCode key in Dict will exist
                return False
        else:
                # Configlet exists, no errorCode key will exist
                return True

def create_new_configlet(configlet_name, filename):
    with open(CONFIGLET_SUBDIR + "/" +filename, "r") as fopen:  # open file in sibdirectory
        configlet_content = fopen.read() # read in the file contents to create configlet
        # Create new Configlet using the file content
        newConfiglet_data = json.dumps({"config": configlet_content, "name": configlet_name})
        newConfiglet_url = "https://%s/cvpservice/configlet/addConfiglet.do" % CVP_HOST
        newConfiglet_response = requests.post(newConfiglet_url, data=newConfiglet_data, cookies=cookies, verify=False)

    return

def update_existing_configlet(configlet_name, filename):
    #call getConfigletByName.do to key keyname value to pass to updateConfiglet.do
    configletbyname_search_url = ('https://%s/cvpservice/configlet/getConfigletByName.do?name=%s' %(CVP_HOST, configlet_name))
    configletbyname_search_response = requests.get(configletbyname_search_url, data=None, cookies=cookies, verify=False)
    configletbyname_json = configletbyname_search_response.json()
    configlet_key = configletbyname_json['key']

    with open(CONFIGLET_SUBDIR + "/" +filename, "r") as fopen:  # open file in sibdirectory
        configlet_content = fopen.read() # read in the file contents to create configlet
        # Create new Configlet using the file content
        newConfiglet_data = json.dumps({"config": configlet_content, "name": configlet_name, "key": configlet_key, "waitForTaskIds": False, "reconciled": False})
        newConfiglet_url = "https://%s/cvpservice/configlet/updateConfiglet.do" % CVP_HOST
        newConfiglet_response = requests.post(newConfiglet_url, data=newConfiglet_data, cookies=cookies, verify=False)
    return


def createconfiglets():
    # Reading config from a file
    # r=root, d=directories, f=files
    for r,d,f in os.walk(CONFIGLET_SUBDIR):
        for filename in f:
            if '.txt' in filename:  # this is a configlet file
                # Remove file extension txt and convert to uppercase
                configlet_name = ''.join(filename.split())[:-4].upper()

                # Determine if configlet already exists <True | False>
                configlet_name_exists = search_for_configlet(configlet_name)
                if configlet_name_exists:
                    update_existing_configlet(configlet_name, filename)
                    print("updating existing configlet: " + configlet_name)
                else:
                    create_new_configlet(configlet_name, filename)
                    print("creating new configlet: " + configlet_name)

def main():
        createconfiglets()

if __name__ == "__main__":
    main()
