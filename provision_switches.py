#!/usr/bin/env python

import requests
import json
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import pprint
import getpass

# Lab - modify for environment
CVP_HOST = "192.168.1.248"

# Prompt user and password
CVP_USER = raw_input("Username: ")
CVP_PWD = getpass.getpass("Password: ")

# SUBNET TO CONFIGLETNAME MAPPING
SUBNET_CONFIGLET_MAPPING = {'10.1.1':'PILOTSTORE-SW1-BASE'}

# SUBNET TO CONTAINER MAPPING
CONTAINER_MAPPING = {'10.1.1':'VSS'}

TASKS_DESCRIPTIONS = []

# To remove warnings
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Login into CVP
auth_data = json.dumps({"userId": CVP_USER, "password": CVP_PWD})
auth_url = "https://%s/cvpservice/login/authenticate.do" % CVP_HOST
auth_response = requests.post(auth_url, data=auth_data, verify=False)
assert auth_response.ok
cookies = auth_response.cookies

def get_switch_configlet_key(switch_mgmt_configlet_name):
        configlet_url = ('https://%s/cvpservice/configlet/getConfigletByName.do?name=%s' %(CVP_HOST, switch_mgmt_configlet_name))
        configlet_response = requests.get(configlet_url, data=None, cookies=cookies, verify=False)
        configlet_list_json = configlet_response.json()

        #if configlet_list_json != []:
        if len(configlet_list_json) > 2:
                return(configlet_list_json['key'])
        else:
                print("Exiting...  Could not find assigned configlet and key.")
                print("Likely need to run createconfiglets script to create and upload configlets into CVP")
                exit()

def get_undefined_switches():
        device_search_url = ('https://%s/cvpservice/inventory/devices' %(CVP_HOST))
        device_search_response = requests.get(device_search_url, cookies=cookies, verify=False)
        device_list_json = device_search_response.json()
        undefined_switches = [] # list of switches we need to configure

        for device in device_list_json:
                if device['parentContainerKey'] == 'undefined_container':
                        undefined_switches.append(device)
        return undefined_switches

def get_target_container_key(target_container_name):
        url_path = ('https://%s/cvpservice/inventory/containers?name=%s' %(CVP_HOST, target_container_name))
        response = requests.get(url_path, cookies=cookies, verify=False)
        container_json = response.json()
        if container_json == []:
            print('{0} container not found.'.format(target_container_name))
            quit()
        else:
            container_key = container_json[0]['Key']
            return(container_key)

def move_switch_to_container(switch,target_container_name,target_container_key):
    undefined_container = 'undefined_container'
    url_path = ('https://%s/cvpservice/provisioning/addTempAction.do?format=topology&queryParam=&nodeId=root' %(CVP_HOST))
    info = 'Move %s to container: %s' % (switch['hostname'], target_container_name)
    payload = {"data": [{
                        "info": info ,
                        "infoPreview": info,
                        "action": "update",
                        "nodeType": "netelement",
                        "nodeId": switch['systemMacAddress'],
                        "toId": target_container_key,
                        "fromId": undefined_container,
                        "nodeName": switch['fqdn'],
                        "toName": target_container_name,
                        "toIdType": "container",
                        "childTasks": [],
                        "parentTask": ""}]}


    response = requests.post(url_path, data=json.dumps(payload), cookies=cookies, verify=False)

    return(info)

def get_current_configlets(switch):
    url_path = ('https://%s/cvpservice/provisioning/getConfigletsByNetElementId.do?netElementId=%s&queryParam=&startIndex=0&endIndex=0' % (CVP_HOST, switch['systemMacAddress']))
    response = requests.get(url_path, cookies=cookies, verify=False)
    response_json = response.json()
    return(response_json['configletList'])

def apply_configlets_to_switch(switch,combined_configlet_names_list,combined_configlet_keys_list):
    url_path = ('https://%s/cvpservice/provisioning/addTempAction.do?format=topology&queryParam=&nodeId=root' % (CVP_HOST))

    info = 'Assign Configlet {0}: to Device {1}'.format(combined_configlet_names_list[-1], switch['hostname'])

    payload = json.dumps({'data': [{'info': info,
                    'infoPreview': info,
                    'note': '',
                    'action': 'associate',
                    'nodeType': 'configlet',
                    'nodeId': '',
                    'configletList': combined_configlet_keys_list,
                    'configletNamesList': combined_configlet_names_list,
                    'ignoreConfigletNamesList': [],
                    'ignoreConfigletList': [],
                    'configletBuilderList': [],
                    'configletBuilderNamesList': [],
                    'ignoreConfigletBuilderList': [],
                    'ignoreConfigletBuilderNamesList': [],
                    'toId': switch['systemMacAddress'],
                    'toIdType': 'netelement',
                    'fromId': '',
                    'nodeName': '',
                    'fromName': '',
                    'toName': switch['fqdn'],
                    'nodeIpAddress': switch['ipAddress'],
                    'nodeTargetIpAddress': switch['ipAddress'],
                    'childTasks': [],
                    'parentTask': ''}]})

    response = requests.post(url_path, data=payload, cookies=cookies, verify=False)




def save_topology():
  url_path = ('https://%s/cvpservice/provisioning/saveTopology.do' % (CVP_HOST))
  data = '[]'
  response = requests.post(url_path, data=data, cookies=cookies, verify=False)

def get_tasks():
        tasklist = []
        url_path = ('https://%s/cvpservice/task/getTasks.do?startIndex=0&endIndex=50' % (CVP_HOST))
        response = requests.get(url_path, cookies=cookies, verify=False)
        response_json = response.json()
        tasks = response_json
        for task in tasks['data']:
            if task['currentTaskName'] == 'Submit':
                tasklist.append(task)
        return tasklist

def execute_tasks(all_tasks,target_task_descriptions):
    tasks_to_execute = []
    url_path = ('https://%s/cvpservice/task/executeTask.do' % (CVP_HOST))

    #Iterate through tasks to look for descriptions (created by 'info' variable in 'move_device_to_container' function)
    #If description is in the list, add task to a list of tasks to execute

    for task in all_tasks:
        if task['description'] in target_task_descriptions:
            tasks_to_execute.append(str(task['workOrderId']))

    payload = {"data": tasks_to_execute}
    response = requests.post(url_path, data=json.dumps(payload), cookies=cookies, verify=False)

def get_container_tree(container_name):
        # Build a dynamic tree of containers the switch is attached to, start at the bottom and work
        # up to Root container Tenant
        top_of_the_tree = False
        # initialize list of containers
        container_tree_ids = []
        while not top_of_the_tree:
                url_path = ('https://%s/cvpservice/inventory/containers?name=%s' % (CVP_HOST, container_name))
                response = requests.get(url_path, cookies=cookies, verify=False)
                container_json = response.json()
                container_key = container_json[0]['Key']
                if container_key == 'root':
                        top_of_the_tree = True
                container_tree_ids.append(container_key)

                # Get Parent Container Name
                url_path = ('https://%s/cvpservice/provisioning/getContainerInfoById.do?containerId=%s' % (CVP_HOST, container_key))
                response = requests.get(url_path, cookies=cookies, verify=False)
                parent_container = response.json()
                parent_container_name = parent_container['parentName']
                container_name = parent_container_name

        return(container_tree_ids)

def build_container_configlet_list(container_tree):

        # Initialize Lists
        container_configlet_names = []
        container_configlet_keys = []

        # reverse container list from Top Down
        for container_key in reversed(container_tree):
                 # https://cvp2.thielnet.com/cvpservice/provisioning/getConfigletsByContainerId.do?containerId=container_90740f0d-ace2-4f07-a03f-fd846a874b84&startIndex=0&endIndex=0
                url_path = ('https://%s/cvpservice/provisioning/getConfigletsByContainerId.do?containerId=%s&startIndex=0&endIndex=0' % (CVP_HOST, container_key))
                response = requests.get(url_path, cookies=cookies, verify=False)
                container_configlets = response.json()
                container_configlet_listing = container_configlets['configletList']
                # There are configlets assigned to containers.  Build the list
                if container_configlet_listing:
                        for configlet in container_configlet_listing:
                                container_configlet_names.append(configlet['name'])
                                container_configlet_keys.append(configlet['key'])

        return container_configlet_names, container_configlet_keys

def configure_switches(switchlist):
        for switch in switchlist:
                # Define Container & Configlet names for undefined switch
                switch_subnet = switch['ipAddress'].split('.')
                switch_subnet = ".".join(switch_subnet[:-1]) # remove 4th octet to get subnet

                # subnet to container = VSS in test case
                # make sure key exists before mapping device to container
                if switch_subnet in CONTAINER_MAPPING.keys():
                        target_container_name = CONTAINER_MAPPING[switch_subnet]
                        # Test changing the container
                        # target_container_name = 'TEST3'


                        # New management configlet to add to the device
                        switch_mgmt_configlet_name = SUBNET_CONFIGLET_MAPPING[switch_subnet]
                        switch_mgmt_configlet_key = get_switch_configlet_key(switch_mgmt_configlet_name)

                        print("Moving device: " + switch['hostname'] + " to container " + target_container_name + " and attaching configlet " + switch_mgmt_configlet_name)

                        target_container_key = get_target_container_key(target_container_name)

                        task_info = move_switch_to_container(switch,target_container_name,target_container_key)

                        TASKS_DESCRIPTIONS.append(task_info)

                        # Build a container tree for which the switch is being attached to.
                        # This is needed to get a list of all configlets attached to the containers part of the tree
                        container_tree = get_container_tree(target_container_name)
                        container_configlet_names, container_configlet_keys = build_container_configlet_list(container_tree)

                        # Combine Container and Switch Configlets into single lists
                        container_configlet_names.append(switch_mgmt_configlet_name)
                        combined_configlet_names_list = container_configlet_names
                        container_configlet_keys.append(switch_mgmt_configlet_key)
                        combined_configlet_keys_list = container_configlet_keys

                        # Now attach all configlets to device
                        apply_configlets_to_switch(switch,combined_configlet_names_list,combined_configlet_keys_list)

def main():

        # Find switches sitting in the undefined container, create a list/array of dictionaries of switches
        switches_to_configure = get_undefined_switches()

        # Move switches in the undefined container to new container and assign all configlets
        if (len(switches_to_configure)) > 0:
                print("Moving switches to final container and attaching configlets")
                # send array of switches
                configure_switches(switches_to_configure)

                # Save the topology after move and configlets are applied
                print("Saving Topology changes")
                save_topology()

                # Get a list of unsubitted tasks
                print("Executing Tasks")
                tasks_to_submit = get_tasks()

                # Execute all tasks generated by this script
                # TASK_DESCRIPTIONS has a list of tasks created buy this script
                execute_tasks(tasks_to_submit,TASKS_DESCRIPTIONS)
                print("Tasks execution complete.\nDevices are rebooting.  Standby for 10 minutes.")
        else:
                print("No devices found to provision.  Exiting.")
                exit()

if __name__ == "__main__":
    main()

