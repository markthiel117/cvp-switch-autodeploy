# Store Pilot CVP Auto-Deploy - WORK IN PROGRESS

![Arista CVP Automation](https://img.shields.io/badge/Arista-CVP%20Automation-blue)

## About

This repository automates the process of onboarding switches into CVP.  New switches will auto-register into CVP via ZTP and land in the undefined container.  The "provision_switches.py" python script will move the switches to proper container based on IP subnet and also attach a base configlet.

There are 3 components to this workflow:
1.  Ansible Playbook (playbook_create_configlets.yml) to generate Base configlets for each switch
2.  Python Script (push_configlets_to_cvp.py) to push generate configlets into CVP
3.  Python Script (provision_switches.py)to provision the switches

## Assumptions

Base configlets for each switch will be present in the ./configlets/ subdirectory.  Each configlet will be named <filename.txt>

## Getting Started running Scripts Manually

From the control host

```shell
# Update CSV with switch info (hostname, ip , mask, gateway)
# Use editor of choice or copy file to ./datafiles/switch_info.csv
vi ./datafiles/switch_info.csv

# Run Playbook to Generate Local configlets
ansible-playbook playbook_create_configlets.yml

# Run Script push configlets to CVP
./push_configlets_to_cvp.py

# Factory Reset Switch to ZTP mode if not already complete

# Run Script to move switches to proper container and attach base configlet
./provision_switches.py

```

## Getting Started running Scripts Automatically

From the control host create a crontab job to run the script every 5 minutes and log output on each run.

```shell
# Create a crontab entry with the following line.  Update the path as needed
*/5 * * * * (/bin/date && /home/cvpadmin/cvp-switch-autodeploy/provision_switches.py) >> /home/cvpadmin/cvp-switch-autodeploy/autodeploy.log 2>&1

```

## Resources

- [CVPRAC](https://github.com/aristanetworks/cvprac)
